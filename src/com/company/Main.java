package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        System.out.println("Bienvenue au Kata Kebab !");

        Scanner sc = new Scanner(System.in);
        boolean stopIngredients = false;
        ArrayList<Ingredient> ingredients = new ArrayList<>();

        String choix = "";

        Pain pain = new Pain(1, "classique");
        Ingredient salade = new Ingredient("salade", Arrays.asList("vege"));
        Ingredient tomate = new Ingredient("tomate", Arrays.asList("vege"));
        Ingredient viande = new Ingredient("Donër", Arrays.asList("viande"));
        Ingredient oignons = new Ingredient("oignons", Arrays.asList("vege"));
        Ingredient crevette = new Ingredient("crevette", Arrays.asList("pisco"));
        Ingredient poisson = new Ingredient("poisson", Arrays.asList("pisco"));
        Ingredient poulet = new Ingredient("poulet", Arrays.asList("viande"));
        Ingredient surimi = new Ingredient("surimi", Arrays.asList("pisco"));

        List<Ingredient> ingredientList = new ArrayList<Ingredient>();
        ingredientList.add(salade);
        ingredientList.add(viande);
        ingredientList.add(tomate);
        ingredientList.add(oignons);
        ingredientList.add(crevette);
        ingredientList.add(poisson);
        ingredientList.add(surimi);
        ingredientList.add(poulet);

        List<Ingredient> ingredientsDansLeKebab = new ArrayList<>();
        boolean ajoutIngredient = true;

        do {
            System.out.println("Quel ingrédient souhaitez vous ajouter ?");
                /*for(int i = 0; i< ingredientList.size() ; i++){
                    System.out.println((i + 1) + " " + ingredientList.get(i).getNom());
                }*/
            System.out.println("1 salade                   5 crevette");
            System.out.println("2 donër                    6 poisson");
            System.out.println("3 tomate                   7 surimi");
            System.out.println("4 oignons                  8 poulet");
            System.out.println(0 +" Stop avec les ingrédients");
            int choixIngredient = Integer.parseInt(sc.nextLine());

                switch (choixIngredient){
                    case 1:
                        if (ingredientsDansLeKebab.contains(ingredientList.get(0))) {
                            ingredientsDansLeKebab.get(0).setQuantite(ingredientsDansLeKebab.get(0).getQuantite()+1);
                        } else {
                            ingredientsDansLeKebab.add(ingredientList.get(0));
                        }

                        break;
                    case 2:
                        if (ingredientsDansLeKebab.contains(ingredientList.get(1))) {
                            ingredientsDansLeKebab.get(1).setQuantite(ingredientsDansLeKebab.get(1).getQuantite()+1);
                        } else {
                            ingredientsDansLeKebab.add(ingredientList.get(1));
                        }
                        break;
                    case 3:
                        if (ingredientsDansLeKebab.contains(ingredientList.get(2))) {
                            ingredientsDansLeKebab.get(2).setQuantite(ingredientsDansLeKebab.get(2).getQuantite()+1);
                        } else {
                            ingredientsDansLeKebab.add(ingredientList.get(2));
                        }
                        break;
                    case 4:
                        if (ingredientsDansLeKebab.contains(ingredientList.get(3))) {
                            ingredientsDansLeKebab.get(3).setQuantite(ingredientsDansLeKebab.get(3).getQuantite()+1);
                        } else {
                            ingredientsDansLeKebab.add(ingredientList.get(3));
                        }
                        break;
                    case 5:
                        if (ingredientsDansLeKebab.contains(ingredientList.get(4))) {
                            ingredientsDansLeKebab.get(4).setQuantite(ingredientsDansLeKebab.get(4).getQuantite()+1);
                        } else {
                            ingredientsDansLeKebab.add(ingredientList.get(4));
                        }
                        break;
                    case 6:
                        if (ingredientsDansLeKebab.contains(ingredientList.get(5))) {
                            ingredientsDansLeKebab.get(5).setQuantite(ingredientsDansLeKebab.get(5).getQuantite()+1);
                        } else {
                            ingredientsDansLeKebab.add(ingredientList.get(5));
                        }
                        break;
                    case 7:
                        if (ingredientsDansLeKebab.contains(ingredientList.get(6))) {
                            ingredientsDansLeKebab.get(6).setQuantite(ingredientsDansLeKebab.get(6).getQuantite()+1);
                        } else {
                            ingredientsDansLeKebab.add(ingredientList.get(6));
                        }
                        break;
                    case 8:
                        if (ingredientsDansLeKebab.contains(ingredientList.get(7))) {
                            ingredientsDansLeKebab.get(7).setQuantite(ingredientsDansLeKebab.get(7).getQuantite()+1);
                        } else {
                            ingredientsDansLeKebab.add(ingredientList.get(7));
                        }
                        break;
                    case 0:
                        ajoutIngredient = false;
                        break;
                    default:
                        break;

                }

        } while(ajoutIngredient);
/*
        System.out.println("Voulez vous supprimer des ingrédients du kebab ? Oui/Non");
        boolean suppressionIngredient = true;
        String choixSuppression = "";
        if (choixSuppression.equals("Oui")){
            do {
                System.out.println("Quel ingrédient souhaitez vous ajouter ?");
                for(int i = 0; i< ingredientList.size() ; i++){
                    System.out.println((i + 1) + ingredientList.get(i).getNom());
                }

                System.out.println(0 +" Stop avec les ingrédients");
                int choixIngredient = Integer.parseInt(sc.nextLine());
                int choixStopAjout = ingredientList.size() + 1;

                switch (choixIngredient){
                    case 1:
                        //TODO incrementer double dose
                        ingredientsDansLeKebab.add(ingredientList.get(0));
                    case 2:
                        //TODO incrementer double dose
                        ingredientsDansLeKebab.add(ingredientList.get(1));
                    case 3:
                        //TODO incrementer double dose
                        ingredientsDansLeKebab.add(ingredientList.get(2));
                    case 4:
                        //TODO incrementer double dose
                        ingredientsDansLeKebab.add(ingredientList.get(3));
                    case 0:
                        //TODO incrementer double dose
                        suppressionIngredient = false;

                }

            } while(suppressionIngredient);
        } */






        Kebab kebabClassique = new Kebab(pain, ingredientsDansLeKebab, "classique");

        List<String> categoriesFinales = kebabClassique.getCategories();
        for (String categorie: categoriesFinales) {
            System.out.println("Votre kebab est : " + categorie);
        }

        System.out.println("Il contient :");
        for (Ingredient oneIngredient: kebabClassique.getIngredients()) {
            System.out.println(oneIngredient.getNom() + " avec " + oneIngredient.getQuantite() + " portion(s)");
        }

    }
}
