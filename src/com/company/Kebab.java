package com.company;

import java.util.ArrayList;
import java.util.List;

public class Kebab {

    private int id;

    private Pain pain;

    private List<Ingredient> ingredients;

    private String nom;

    private String sauce;

    public Kebab(Pain pain, List<Ingredient> ingredients, String nom){
        this.pain = pain;
        this.ingredients = ingredients;
        this.nom = nom;
    }


        public List<String> getCategories(){
        List<String> categorieFinal = new ArrayList<>();
            for (Ingredient ingredient: this.ingredients) {

                for (String categorie: ingredient.getCategorie()){
                    if (!categorieFinal.contains(categorie)) {
                        categorieFinal.add(categorie);
                    }
                }
            }
            if(categorieFinal.contains("viande")){
                categorieFinal.removeIf(categorie -> categorie.equals("vege"));
                categorieFinal.removeIf(categorie -> categorie.equals("pisco"));
            }
            if(categorieFinal.contains("pisco")){
                categorieFinal.removeIf(categorie -> categorie.equals("vege"));
            }
            return categorieFinal;
        }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Pain getPain() {
        return pain;
    }

    public void setPain(Pain pain) {
        this.pain = pain;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getSauce() {
        return sauce;
    }

    public void setSauce(String sauce) {
        this.sauce = sauce;
    }
}
